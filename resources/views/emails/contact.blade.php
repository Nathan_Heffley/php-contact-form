@component('mail::message')
# New Contact Form Submission

From: {{ $submission->name }} &lt;[{{ $submission->email }}](mailto:{{ $submission->email }})&gt;

@isset($submission->phone)
Phone: {{ $submission->phone }}
@endisset

{{ $submission->message }}
@endcomponent
