<?php

namespace App\Http\Controllers;

use App\Submission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubmissionsController extends Controller
{
    public function submit(Request $request)
    {
        $submission = new Submission($request->all());

        if (! $submission->persist()) {
            return response()->json([
                'errors' => $submission->errors
            ], 400);
        }

        $submission->sendEmail();

        return response()->json([
            'flash' => 'Thank you for contacting us!'
        ]);
    }
}
