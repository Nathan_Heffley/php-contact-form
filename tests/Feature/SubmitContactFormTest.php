<?php

namespace Tests\Feature;

use App\Submission;
use Tests\TestCase;
use App\Mail\ContactFormSubmission;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubmitContactFormTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        Mail::fake();
    }

    /** @test */
    public function submittingContactFormSendsEmailAndSavesToDatabase()
    {
        $data = [
            'name'    => 'John Smith',
            'email'   => 'john@example.com',
            'phone'   => '(555) 555-1234',
            'message' => 'This is a message'
        ];

        $response = $this->json('POST', '/contact', $data);

        $response
            ->assertStatus(200)
            ->assertJson([
                'flash' => 'Thank you for contacting us!'
            ]);

        $this->assertDatabaseHas('submissions', $data);

        Mail::assertSent(ContactFormSubmission::class, function ($email) {
            return $email->hasTo(env('TEST_CONTACT_EMAIL'));
        });
    }

    /** @test */
    public function submittingContactFormWithoutNameFails()
    {
        $this->withoutExceptionHandling();

        $response = $this->json('POST', '/contact', [
            'name'    => null,
            'email'   => 'john@example.com',
            'message' => 'This is a message'
        ]);

        $response
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'name' => ['The name field is required.']
                ]
            ]);

        Mail::assertNotSent(ContactFormSubmission::class);
    }

    /** @test */
    public function submittingContactFormWithoutEmailFails()
    {
        $response = $this->json('POST', '/contact', [
            'name'    => 'John Smith',
            'email'   => null,
            'message' => 'This is a message'
        ]);

        $response
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'email' => ['The email field is required.']
                ]
            ]);

        Mail::assertNotSent(ContactFormSubmission::class);
    }

    /** @test */
    public function submittingContactFormWithoutMessageFails()
    {
        $response = $this->json('POST', '/contact', [
            'name'    => 'John Smith',
            'email'   => 'john@example.com',
            'message' => null
        ]);

        $response
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'message' => ['The message field is required.']
                ]
            ]);

        Mail::assertNotSent(ContactFormSubmission::class);
    }
}
