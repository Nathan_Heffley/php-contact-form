<?php

namespace App;

use App\Mail\ContactFormSubmission;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Submission extends Model
{
    public $errors = null;

    protected $fillable = [
        'name', 'email', 'phone', 'message'
    ];

    public function sendEmail()
    {
        // Normally I'd put the to email as an environment/config option
        // but I didn't want to make your testers do unnecessary setup.
        Mail::to('guy-smiley@example.com')->send(new ContactFormSubmission($this));
    }

    public function persist()
    {
        $validator = Validator::make($this->toArray(), [
            'name'    => 'required|max:255',
            'email'   => 'required|email|max:255',
            'phone'   => 'max:50',
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            $this->errors = $validator->errors()->toArray();
            return false;
        }

        $this->save();

        return true;
    }
}
