<?php

namespace Tests\Unit;

use App\Submission;
use Tests\TestCase;
use App\Mail\ContactFormSubmission;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function submissionCanSendEmailToNotificationAddress()
    {
        Mail::fake();

        $submission = factory('App\Submission')->make();

        $submission->sendEmail();

        Mail::assertSent(ContactFormSubmission::class, function ($email) {
            return $email->hasTo(env('TEST_CONTACT_EMAIL'));
        });
    }

    /** @test */
    public function submissionCanValidateAndSaveItself()
    {
        $goodData = [
            'name'    => 'John Smith',
            'email'   => 'john@example.com',
            'phone'   => '(555) 555-1234',
            'message' => 'This is a message'
        ];

        $badSubmission = factory('App\Submission')->make([
            'name'    => null,
            'email'   => 'notanemailaddress',
            'message' => 'This is a bad submission'
        ]);
        $goodSubmission = factory('App\Submission')->make($goodData);

        $badStatus  = $badSubmission->persist();
        $goodStatus = $goodSubmission->persist();

        $this->assertFalse($badStatus, 'An invalid submission claims it was persisted.');
        $this->assertTrue($goodStatus, 'A valid submission claims it was not persisted.');

        $this->assertDatabaseMissing('submissions', [
            'email'   => 'notanemailaddress',
            'message' => 'This is a bad submission'
        ]);

        $this->assertDatabaseHas('submissions', $goodData);
    }

    /** @test */
    public function anInvalidSubmissionHasErrorsAfterPersistFails()
    {
        $submission = factory('App\Submission')->make([
            'name'    => null,
            'email'   => 'john@example.com',
            'message' => 'This is a message'
        ]);

        $submission->persist();

        $this->assertEquals('The name field is required.', $submission->errors['name'][0]);
    }
}
